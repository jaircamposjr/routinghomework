The file provided is nodes.txt.  It should have the output of 

Table for u
  v (2,v)
  w (3,x)
  x (1,x)
  y (2,x)
  z (4,x)
Table for v
  u (2,u)
  w (3,w)
  x (2,x)
  y (3,x)
  z (5,x)
Table for w
  u (3,y)
  v (3,v)
  x (2,y)
  y (1,y)
  z (3,y)
Table for x
  u (1,u)
  v (2,v)
  w (2,y)
  y (1,y)
  z (3,y)
Table for y
  u (2,x)
  v (3,x)
  w (1,w)
  x (1,x)
  z (2,z)
Table for z
  u (4,y)
  v (5,y)
  w (3,y)
  x (3,y)
  y (2,y)