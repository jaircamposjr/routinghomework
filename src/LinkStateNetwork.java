/**
 * @author Wade Ashby
 * @version 3/20/2018
 */

public class LinkStateNetwork {
    // These attributes were ones I used to solve the problem but you may not need them all.
    private int len;                // This is the key to arrays staying in parallel
    private int[][] nodeMatrix;     // 2D Matrix of int values used to find next hop in network
    private String[][] pathToNode;  // 2D Matrix of the paths from one node to another
    private int[][] pathToNodeCost; // 2D matrix of the cost of the path from each node to another
    private String[] nodeNames;     // The node names for the network
    private String[] connections;   // The connection information for the network
    private int[][] connects;

    private int testingNode = 1;

    /**
     * Builds the LinkStateNetwork which will build routing tables for all nodes in the network.
     * Use the NetworkUtils class to parse the file and build the nodeNames and connections.
     *
     * @param nodeNames The names of all the nodes in the network
     * @param connections The connection information for the network
     */
    public LinkStateNetwork(String[] nodeNames, String[] connections) {
        this.len = nodeNames.length;
        this.connections = connections;
        this.nodeNames = nodeNames;
        this.nodeMatrix = new int[len][len];
        this.pathToNode = new String[len][len];
        this.pathToNodeCost = new int[len][len];
        this.connects = new int[len][len];
        // Set the default for the values to the largest possible value.
        for (int x = 0; x < len; x++) {
            for (int y = 0; y < len; y++) {
                this.nodeMatrix[x][y] = Integer.MAX_VALUE;
            }
        }
        //split the nodes
        
        
        for(int i=0; i<len; i++){
         String temp[]= connections[i].split("\\),");
         temp[temp.length-1] = temp[temp.length-1].substring(0,temp[temp.length-1].length()-1);
         for(int j = 0; j<temp.length; j++){
            int num = Integer.parseInt(temp[j].split(",")[i]);
            String node = temp[j].split(",")[0].substring(i);
            for(int k = 0; k<len; k++){
               if(nodeNames[k].equals(node)){
                  connects[i][k] = num;
                  break;
               }
            }
         }
        }
        
        for (int i = 0; i < len; i++) {
            buildRoutingTableForNode(i);
        }
    }

    /**
     * Accepts an integer that will be the index for the node to map from nodeNames. Then
     * it will build the routing table using the Link-State algorithm.
     *
     * @param nodeToMap int for the index of the node to map
     */
    private void buildRoutingTableForNode(int nodeToMap){
       // FixMe: Write code to build routing table given a node. A couple of helper functions are provided
       // Generates a boolean array for each node to see if it has been moved to nPrime.
        boolean[] nPrime = new boolean[this.len];
        
        int cost=0;
        int i=0;
        int j=0;
        
        nPrime[nodeToMap] = true;
        int currentNode = nodeToMap;
        while(!nEqualsNPrime(nPrime)) {
         int least = Integer.MAX_VALUE;
         int lNode = 0;
         for(boolean nPrimeElement : nPrime){
            if(!nPrimeElement){
               if(i>0){
                  if(connects[currentNode][j]<Integer.MAX_VALUE && connects[currentNode][j]+cost<nodeMatrix[i-1][j]){
                     nodeMatrix[i][j] = connects[currentNode][j]+cost;
                     pathToNode[nodeToMap][j]=nodeNames[currentNode];
                  }
                  else{
                     nodeMatrix[i][j] = nodeMatrix[i-1][j];
                  }
                  
               }
            
            else if(i==0){
               if(connects[currentNode][j]<Integer.MAX_VALUE){
                  nodeMatrix[i][j] = connects[currentNode][j];
                  pathToNode[nodeToMap][j] = nodeNames[currentNode];
               }
            }
            if(nodeMatrix[i][j]<least){
               least = nodeMatrix[i][j];
               lNode = j;
            }
         }
         else if(i>0){
            nodeMatrix[i][j] = nodeMatrix[i-1][j];
         }
         j++;
        }
        pathToNodeCost[nodeToMap][lNode] = least;
        nPrime[lNode] = true;
        currentNode= lNode;
        cost = least;
        
        j=0;
        i++;
    }
 }

    /**
     *
     * @param nodeFromString String of the node at the start of the connection
     * @param nodeToString String of the node at the end of the connection
     * @return an integer of the cost between the two nodes
     */
    private int connectionCostLookup(String nodeFromString, String nodeToString){
        int nodeFromInt = findNodeByName(nodeFromString);
        String connectionInfo = this.connections[nodeFromInt];
        connectionInfo = connectionInfo.replace("(","").replace(")","");
        String[] nodeInfoPair = connectionInfo.split(",");
        for(int i=0; i<nodeInfoPair.length; i+=2){
            if(nodeInfoPair[i].equalsIgnoreCase(nodeToString)){
                return Integer.parseInt(nodeInfoPair[i+1]);
            }
        }
        return Integer.MAX_VALUE;
    }

    /**
     * Method that given nPrime will check to see if all the values of nPrime are true.
     *
     * @param nPrime The array of boolean values that parallels the node names
     * @return true if all values in nPrime are true otherwise false
     */
    private boolean nEqualsNPrime(boolean[] nPrime){
       // This is a for-each loop. Meaning it is shorthand for each element in the array nPrime.
        for (boolean nPrimeElement : nPrime) {
            if (!nPrimeElement)
                return false;
        }
        return true;
    }

    /**
     * Returns the index of a node in the network given the name of the node. This is provided
     * for easy lookup since the arrays are in parallel.
     *
     * @param name String of the name of a node in the network
     * @return int of the index of the node with -1 if not found
     */
    private int findNodeByName(String name){
        for(int i=0; i<len; i++){
            if(name.equalsIgnoreCase(this.nodeNames[i]))
                return i;
        }
        return -1;
    }

    /**
     * Creates printable string of the routing tables for all nodes in the network
     *
     * @return returns printable string
     */
    @Override
    public String toString(){
        String result = "";
        for(int i=0; i<len; i++){
            result += "Routing Table for node "+this.nodeNames[i]+"\n";
            for(int j=0; j<len; j++){
                if(j!=i){
                    result += "  " + this.nodeNames[j];
                    if(this.pathToNode[i][j] == null){
                        result += " is not set.\n";
                    }else {
                        result += "  (" + this.pathToNodeCost[i][j] + ",";
                        if (this.pathToNode[i][j].length() == 1) {
                            result += this.nodeNames[j];
                        } else {
                            int len = this.pathToNode[i][j].length();
                            result += this.pathToNode[i][j].charAt(len-1);
                        }
                        result += ")\n";
                    }
                }
            }
        }
        return result;
    }
}