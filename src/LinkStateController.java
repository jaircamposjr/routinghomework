/**
 * @author Wade Ashby
 * @version 3/20/2018
 */
import java.io.*;
import java.util.*;
public class LinkStateController {
    /**
     * Main method to start the LinkStateController
     * @param args No command line parameters needed or used
     */
    public static void main(String[] args)throws FileNotFoundException{
//         Setup networkInfo as NetworkUtils object to handle attributes of the network
        NetworkUtils networkInfo = new NetworkUtils("nodes.txt");
//         Setup LinkStateNetwork by passing the Node Names and Connection Info
        LinkStateNetwork net = new LinkStateNetwork(networkInfo.getNodeNames(), networkInfo.getConnections());
        System.out.println(net);
    }
}
